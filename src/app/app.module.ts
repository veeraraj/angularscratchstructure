import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppComponent } from './app.component';

import {
  TopHeaderComponent,
  FooterComponent,
  MainComponent,
  NavbarComponent,
  ErrorComponent
} from './layouts';
import { DashboardComponent } from './dashboard/dashboard.component';
import { EmployerComponent } from './employer/employer.component';
import { BusinessComponent } from './business/business.component';
import { DownloadsComponent } from './downloads/downloads.component'
import { DashboardModule } from './dashboard/dashboard.module';
import { EmployerModule } from './employer/employer.module';
import { BusinessModule } from './business/business.module'
import { DownloadsModule } from './downloads/downloads.module';
import {
  LayoutRoutingModule,
} from './layouts/layout-routing.module';

@NgModule({
  declarations: [
    AppComponent,
    TopHeaderComponent,
    FooterComponent,
    MainComponent,
    NavbarComponent,
    ErrorComponent,
    DashboardComponent,
    EmployerComponent,
    BusinessComponent,
    DownloadsComponent
  ],
  imports: [
    LayoutRoutingModule,
    BrowserModule,
    DashboardModule,
    EmployerModule,
    BusinessModule,
    DownloadsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
