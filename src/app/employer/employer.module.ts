import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { EMPLOYER_ROUTE } from './employer.route';

@NgModule({
  imports: [
    RouterModule.forRoot(EMPLOYER_ROUTE)
  ],
  declarations: [
  ]
})
export class EmployerModule { }