import { Routes } from "@angular/router";
import { EmployerComponent } from './employer.component';
import { MainComponent } from "../layouts";

// export const EMPLOYER_ROUTE: Routes = [{
// 	path: 'employer',
//     component: EmployerComponent
// }]

export const EMPLOYER_ROUTE: Routes = [
	{
		path: '',
		component: MainComponent,
		children: [
			{
				path: 'employer',
				component: EmployerComponent,
			}
		]
	}
];