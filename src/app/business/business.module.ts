import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BUSINESS_ROUTE } from './business.route';


@NgModule({
  imports: [
    RouterModule.forRoot(BUSINESS_ROUTE)
  ],
  declarations: []
})
export class BusinessModule { }