import { Routes } from "@angular/router";
import { BusinessComponent } from './business.component';
import { MainComponent } from '../layouts';

export const BUSINESS_ROUTE: Routes = [
    {
        path: '',
        component: MainComponent,
        children: [
            {
                path: 'business',
                component: BusinessComponent,
            }
        ]
    }
];