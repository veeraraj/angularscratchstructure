import { Routes } from "@angular/router";
import { DownloadsComponent } from './downloads.component';
import { MainComponent } from "../layouts";

export const DOWNLOAD_ROUTE: Routes = [
	{
		path: '',
		component: MainComponent,
		children: [
			{
				path: 'downloads',
				component: DownloadsComponent,
			}
		]
	}
];