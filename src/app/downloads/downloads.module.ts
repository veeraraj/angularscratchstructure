import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DOWNLOAD_ROUTE } from './downloads.route';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(DOWNLOAD_ROUTE)
  ],
})
export class DownloadsModule { }
