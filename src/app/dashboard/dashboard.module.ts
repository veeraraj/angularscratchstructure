import { NgModule,  } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HOME_ROUTE } from './dashboard.route';

@NgModule({
  imports: [
    RouterModule.forRoot(HOME_ROUTE)
  ],
  declarations: [
  ]
})
export class DashboardModule { }



