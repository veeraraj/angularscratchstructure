import { Routes } from "@angular/router";
import { DashboardComponent } from './dashboard.component';

import { MainComponent } from '../layouts';

export const HOME_ROUTE: Routes = [
	{
		path: '',
		component: MainComponent,
		children: [
			{
				path: '',
				component: DashboardComponent,
			}
		]
	}
];