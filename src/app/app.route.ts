import { Route } from '@angular/router';

import { NavbarComponent, TopHeaderComponent } from './layouts';

export const navbarRoute: Route = {
	path: '',
	component: NavbarComponent,
	outlet: 'navbar'
};

export const topHeaderRoute: Route = {
	path: '',
	component: TopHeaderComponent,
	outlet: 'top-header'
};
