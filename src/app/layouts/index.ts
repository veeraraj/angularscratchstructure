export * from './footer/footer.component';
export * from './main/main.component';
export * from './navbar/navbar.component';
export * from './top-header/top-header.component';
export * from './error/error.component';
export * from './error/error.route';
// export * from '../layouts/layout-routing.module';